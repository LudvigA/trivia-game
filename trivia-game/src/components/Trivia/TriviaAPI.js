const apiURL = 'https://opentdb.com/api'

/*Fetches questions, categories and tokens from opentdb API*/
export const TriviaAPI = {
  async getQuestions(params) {
    try {
      const response = await fetch(`${apiURL}.php?${params.toString()}`);
      const result = await response.json();

      return [null, result];
    } catch (error) {
      return [error.message, []];
    }
  },
      async getCategories() {
        try {
          const categories = await fetch(`${apiURL}_category.php`);
          const { trivia_categories } = await categories.json();
    
          return [null, trivia_categories];
        } catch (error) {
          return [error.message, []];
        }
      },
    
      async getToken() {
        try {
          const response = await fetch(`${apiURL}_token.php?command=request`);
          const result = await response.json();
          return [null, result];
        } catch (error) {
          return [error.message, []];
        }
      },  
};