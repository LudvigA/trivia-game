const apiURL = 'https://assignemnt5-triviagame.herokuapp.com';
const apiKey = 'potet';

/*Fetches users from noroff Triva API*/
export const LoginAPI = {

   async register(details) {
        
        const requestOptions = {
            method: "POST",
            headers: {
                "Content-Type" : "application/json",
                "X-API-KEY" : apiKey,
            },
            body: JSON.stringify({
                
                username: details.username,
                highscore: 0
            }),
        };

        try {
            const user = await fetch(`${apiURL}/trivia`, requestOptions);
            if (!user.ok){
                throw new Error("Could not create new user.");
            }
            return await user.json();
        } catch (error) {
            console.error(error);
            
        }
    },

   async login(details){
       try {
           const user = await fetch(`${apiURL}/trivia?username=${details.user}`);
           
           if(!user.ok){
               throw new Error("Could not retrieve user");
           }
           return await user.json();
           
       } catch (error) {
           console.error(error);
           
       }
    },

    async updateHighScore(userId, score) {
        const requestOptions = {
          method: "PATCH",
          headers: {
            "X-API-Key": apiKey,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            highscore: score,
          }),
        };
        try {
          const response = await fetch(
            `${apiURL}/trivia/${userId}`,
            requestOptions
          );
          if (!response.ok) {
            throw new Error("Could not update highscore");
          }
        } catch (error) {
          console.error(error);
        }
      },
}