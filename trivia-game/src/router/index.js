import VueRouter from 'vue-router'
import Vue from 'vue'

Vue.use(VueRouter)

/*Routes for application, uses lazy loading.*/
const routes = [
    {
        path:'/',
        name: 'StartScreen',
        component: () => import(/* webpackChunkName: "login"*/'../components/StartScreen/StartScreen.vue')
    },
    {
        path: '/questions',
        name: 'Questions',
        component: () => import(/* webpackChunkName: "questions"*/'../components/Trivia/Questions.vue')
    },
    {
        path: '/result',
        name: 'Result',
        component: () => import(/* webpackChunkName: "setDetails"*/'../components/Resultscreen/Result.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes

})
export default router;