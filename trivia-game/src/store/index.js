import Vue from 'vue'
import Vuex from 'vuex'
import { TriviaAPI } from '../components/Trivia/TriviaAPI'


Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        user: '',
        score: '',
        questions: [],
        categories: [''],
        error: '',
        token: '',
        isLoading: true,
        selectedCategory: '',
        numberOfQs: 3,
        difficulty: 'easy',
        questionIndex: 0
    },

    mutations:{
        setUser:(state,payload) =>{
            state.user = payload
        },
        setError:(state,payload) =>{
            state.error = payload
        },
        setScore:(state,payload) =>{
            state.score = payload
        },
        setQuestions:(state, payload) =>{
            state.questions = payload
        },       
        setAnswer:(state,payload) =>{
            state.questions[state.questionIndex].answer = payload
        },
        setCategories:(state, payload) =>{
            state.categories = payload
        },
        setToken:(state, payload) => {
            state.token = payload
        },
        setIsLoading: (state, payload) => {
            state.isLoading = payload
        },
        setSelectedCategory: (state, payload) => {
            state.selectedCategory = payload
        },
        setNumberOfQs: (state, payload) => {
            state.numberOfQs = payload
        },
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        setQuestionIndex: (state, payload) => {
            state.questionIndex = payload
        }
    },

    getters:{
        triviaAnswers: (state) => {
            const triviaAnswers = [
              ...state.questions[state.questionIndex].incorrect_answers,
              state.questions[state.questionIndex].correct_answer,
            ];
            const randomizer = (answer) => {
              for (let i = answer.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [answer[i], answer[j]] = [answer[j], answer[i]];
              }
              return answer;
            };
            return randomizer(triviaAnswers);
          },
      
        maxScore: (state) =>{
            return state.questions.length * 10
        }      
    },

    actions:{

        /**
        * getTriviaToken fetches token from the opentdb api.
        * If the response code equals 0, the token value is set in state, if not an error is set.
        */
        async getTriviaToken ({commit}) {
            try {
                const [error,response] = await TriviaAPI.getToken()

                if(response.response_code === 0) {
                    commit('setToken', response.token)
                } else {
                    commit('setError', `Error getting token ${error}`)
                }           
            } catch (e) {
                commit('setError', e.message)
            }
        },

        /**
        * getTriviaQuestions sets isLoading to ture, while the data is being fetched.
        * Resets questionIndex to 0.
        * If there is no token, fetch new token.
        * Create urlOptions to parse through URLSearchPArams object
        * Set question to be question from response
        * 
        * If an error occurs, set error to be e.message.
        * When finished, set isLoading to false
        */
        async getTriviaQuestions({state, commit}) {

            commit('setIsLoading', true)
            commit('setQuestionIndex', 0)
            commit('setError', '')

            if (state.token === '') {
                await this.dispatch('getTriviaToken')
            }
            try {
                const urlOptions = {
                    amount: state.numberOfQs,
                    difficulty: state.difficulty,
                    category: state.selectedCategory,
                    token: state.token
                };

                const params = new URLSearchParams(urlOptions)
                const response = await TriviaAPI.getQuestions(params)
                commit('setQuestions', response[1].results)
            } catch (e) {
                commit('setError', e.message)
                commit('setIsLoading', false)
            }
            commit('setIsLoading', false)
        },

        /**
         * getTriviaCategories sets isLoading to ture, while the data is being fetched.
         * Set question to be question from response
         * If an error occurs, set error to be e.message.
         * When finished, set isLoading to false
         */
        async getTriviaCategories ({commit}) {
            commit('setIsLoading', true)
            
            try {
                const [error,categories] = await TriviaAPI.getCategories()

                if (categories) {
                    commit('setCategories', categories)
                } else {
                    commit('setError', error)
                }               
            } catch (e) {
                commit('setError', e.message)
            }
            commit('setIsLoading', false)
        },

        /*Increase questionIndex by 1*/
        nextQuestion({state,commit}){
            commit('setQuestionIndex', state.questionIndex + 1);
        }
    }
})