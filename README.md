# Trivia-Game

Single page application made by using Vue.Framework

The application can be launched from: https://safe-crag-32648.herokuapp.com/

## Description
This application is a quiz application consisting of three main functionalities:
A Start screen, a Question Screen and a Result screen.


## Functionality

### Start screen
presents the user with the option to login or register a user. when the user is logged in they are prompted with an option to set the amount of questions they want, category of questions and their difficulity. Press the start button to continue.

### Question screen 
Presents the user with questions fetched from the opendb abi based on their given details. The questions are presented one by one and routes the user to the result screen when all questions have been answered.

### Result screen 
This screen presents the user with all the right answers in green, if the users answer were incorrect the answer is colored red. The user is also presented with their score and maximum score. If the user gets a new highscore, the highscore is recorded in the database and presented when they log in again.

## API
#### Trivia API
Api used to store user information and used to verify or register users when logging in.
#### Opentdb
This API allows the user to fetch questions and categories. The questions are returned as objects which can be displayed in the application.


## Contributers
    * Ludvig Ånestad
    * Sunniva Blom Stolt-Nielsen

